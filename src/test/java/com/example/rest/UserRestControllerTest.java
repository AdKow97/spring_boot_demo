package com.example.rest;

import com.example.SpringBootDemoApplication;
import com.example.model.User;
import com.example.service.UserService;
import org.hamcrest.CoreMatchers;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.platform.engine.TestExecutionResult;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
@AutoConfigureMockMvc(addFilters = false)
//@ContextConfiguration(classes = { SpringBootDemoApplication.class })
//@WebMvcTest(value = UserRestController.class)
//@ExtendWith({MockitoExtension.class, SpringExtension.class})
class UserRestControllerTest {

    private static final long ID = 1L;
    private static final String EMAIL = "myemail@gmail.com";
    private static final String LOGIN = "mylogin";
    private static final String PASSWORD = "password";
    private static final String FIRST_NAME = "Jan";
    private static final String LAST_NAME = "Kowalski";

    @Mock
    private UserService service;
    @Autowired
    private MockMvc mockMvc;

    @Test
    void shouldReturnFoundUserDto() throws Exception {
        // given
        final User user = getUser();
        Mockito.when(service.find(ID)).thenReturn(user);
        RequestBuilder requestBuilder = MockMvcRequestBuilders
                .get("/rest/users/{id}", ID)
                .accept(MediaType.APPLICATION_JSON);
        // when
        // then
        mockMvc.perform(requestBuilder)
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.email", CoreMatchers.is(user.getEmail())))
                .andExpect(MockMvcResultMatchers.jsonPath("$.login", CoreMatchers.is(user.getLogin())))
                .andExpect(MockMvcResultMatchers.jsonPath("$.id", CoreMatchers.is(user.getId())))
                .andExpect(MockMvcResultMatchers.jsonPath("$.password", CoreMatchers.is(user.getPassword())))
                .andExpect(MockMvcResultMatchers.jsonPath("$.firstName", CoreMatchers.is(user.getFirstName())))
                .andExpect(MockMvcResultMatchers.jsonPath("$.lastName", CoreMatchers.is(user.getLastName())));
        Mockito.verify(service).find(ID);
    }

    @Test
    void shouldDeleteUser() throws Exception {
        // given
        Mockito.doNothing().when(service).delete(ID);
        RequestBuilder requestBuilder = MockMvcRequestBuilders
                .delete("/rest/users/{id}", ID)
                .accept(MediaType.APPLICATION_JSON);
        // when
        // then
        mockMvc.perform(requestBuilder)
                .andExpect(MockMvcResultMatchers.status().isAccepted());
        Mockito.verify(service).delete(ID);
    }

    private static User getUser() {
        User user = new User();
        user.setId(ID);
        user.setLogin(LOGIN);
        user.setPassword(PASSWORD);
        user.setEmail(EMAIL);
        user.setFirstName(FIRST_NAME);
        user.setLastName(LAST_NAME);
        return user;
    }
}