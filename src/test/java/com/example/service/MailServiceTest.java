package com.example.service;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;

import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.any;
import static org.mockito.Mockito.verify;

import static org.junit.jupiter.api.Assertions.assertEquals;

@ExtendWith(MockitoExtension.class)
public class MailServiceTest {

    @Mock
    private JavaMailSender sender;
    @InjectMocks
    private MailService service;

    @Test
    void shouldSendMessage() {
        // given
        String to = "mail_to@gmail.com";
        String subject = "my_subject";
        String content = "This is a simple test!";
        doNothing().when(sender).send(any(SimpleMailMessage.class));
        ArgumentCaptor<SimpleMailMessage> messageCapture = ArgumentCaptor.forClass(SimpleMailMessage.class);
        // when
        service.send(to, subject, content);
        // then
        verify(sender).send(messageCapture.capture());
        SimpleMailMessage message = messageCapture.getValue();
        if(message != null) {
            assertEquals(to, message.getTo()[0]);
            assertEquals(subject, message.getSubject());
            assertEquals(content, message.getText());
            assertEquals("Adam_Kowalewski", message.getFrom());
        }
    }
}
