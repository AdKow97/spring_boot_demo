//package com.example.repository;
//
//import com.example.model.EmailConfirmationToken;
//import org.springframework.data.jpa.repository.JpaRepository;
//import org.springframework.stereotype.Repository;
//
//@Repository
//public interface EmailConfirmationTokenRepository extends JpaRepository<EmailConfirmationToken, String> {
//
//    EmailConfirmationToken findByEmailConfirmationToken(String emailConfirmationToken);
//}
