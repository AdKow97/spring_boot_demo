package com.example.controller;

import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletResponse;

@ControllerAdvice
public class ExceptionController {

    @ExceptionHandler(RuntimeException.class)
    public ModelAndView handleInternalServerException(HttpServletResponse response,
                                                      Model model,
                                                      RuntimeException exception) {
        model.addAttribute("errorMsg", exception.getMessage());
        return new ModelAndView("error/error-500", model.asMap());
    }
}
