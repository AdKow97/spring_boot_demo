package com.example.controller;

import com.example.dto.CreateOrUpdateUserDto;
import com.example.mappers.UserMapper;
//import com.example.model.EmailConfirmationToken;
import com.example.model.User;
//import com.example.repository.EmailConfirmationTokenRepository;
import com.example.repository.UserRepository;
//import com.example.service.EmailService;
import com.example.service.UserService;
import org.springframework.beans.propertyeditors.StringTrimmerEditor;
//import org.springframework.mail.SimpleMailMessage;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.oauth2.core.oidc.user.DefaultOidcUser;
import org.springframework.security.oauth2.core.user.DefaultOAuth2User;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("/users") //kolejność adnotacji nie ma znaczenia
public class UserController {

    private final UserService userService;
    private final PasswordEncoder encoder;
//    private UserRepository userRepository;
//    private EmailConfirmationTokenRepository emailConfirmationTokenRepository;
//    private EmailService emailService;

    public UserController(UserService userService, PasswordEncoder encoder) {
        this.userService = userService;
        this.encoder = encoder;
    }


//    public UserController(UserService userService,
//                          UserRepository userRepository,
//                          EmailConfirmationTokenRepository emailConfirmationTokenRepository,
//                          EmailService emailService) {
//        this.userService = userService;
//        this.userRepository = userRepository;
//        this.emailConfirmationTokenRepository = emailConfirmationTokenRepository;
//        this.emailService = emailService;
//    }
//
//    @RequestMapping(value = "/register", method = RequestMethod.GET)
//    public ModelAndView displayRegistration(ModelAndView modelAndView,
//                                            @PathVariable("userId") Long id){
//        CreateOrUpdateUserDto dto = UserMapper.INSTANCE.toDto(userService.findWithAddresses(id));
//        modelAndView.addObject("dto", dto);
//        modelAndView.setViewName("register");
//        return modelAndView;
//    }

//    @RequestMapping(value = "/register", method = RequestMethod.POST)
//    public ModelAndView registerUser(ModelAndView modelAndView,
//                                     @ModelAttribute("dto") CreateOrUpdateUserDto dto){
//        User existingUser = UserMapper.INSTANCE.toEntity(dto);
//        String existingEmail = userRepository.findByEmailIdIgnoreCase(existingUser.getEmail());
//        if(existingUser != null) {
//            modelAndView.addObject("message", "This email already exists!");
//            modelAndView.setViewName("error");
//        } else {
//            userRepository.save(user);
//            EmailConfirmationToken confirmationToken = new EmailConfirmationToken(user);
//            emailConfirmationTokenRepository.save(confirmationToken);
//            SimpleMailMessage mailMessage = new SimpleMailMessage();
//            mailMessage.setTo(user.getEmail());
//            mailMessage.setSubject("Complete registration");
//            mailMessage.setFrom("twój_adres_email");
//            mailMessage.setText("To confirm your account, click here : "
//                            + "http://localhost:8080/confirm-account?token-"
//                            + confirmationToken.getConfirmationToken());
//            emailService.sendEmail(mailMessage);
//            modelAndView.addObject("email", user.getEmail());
//            modelAndView.setViewName("successFullRegistration");
//        }
//        return modelAndView;
//    }

//    @RequestMapping(value = "/confirm-account", method = {RequestMethod.GET, RequestMethod.POST})
//    public ModelAndView confirmUserAccount(ModelAndView modelAndView, @RequestParam("token")String confirmationToken) {
//        EmailConfirmationToken token = emailConfirmationTokenRepository.findByEmailConfirmationToken(confirmationToken);
//        if(token != null) {
//            User user = userRepository.findByEmailIdIgnoreCase(token.getUser().getEmail());
//            user.setActivated(true);
//            userRepository.save(user);
//            modelAndView.setViewName("accountVerified");
//        } else {
//            modelAndView.addObject("message", "The link is invalid or broken");
//            modelAndView.setViewName("error");
//        }
//        return modelAndView;
//    }

    @GetMapping
    public String index(Model model, @ModelAttribute("successMsg") String successMsg) {
        String username = ((DefaultOAuth2User) SecurityContextHolder.getContext().getAuthentication().getPrincipal())
                .getAttribute("name");
//        String username = ((org.springframework.security.core.userdetails.User)
//                SecurityContextHolder.getContext().getAuthentication().getPrincipal())
//                .getUsername();
        model.addAttribute("loggedUser", username);
        model.addAttribute("users", userService.findAll());
        model.addAttribute("successMsg", successMsg);
        return "index";
    }

    @GetMapping("/{userId}")
    public String showUpdateUserForm(Model model, @PathVariable("userId") Long id) {
        CreateOrUpdateUserDto dto = UserMapper.INSTANCE.toDto(userService.findWithAddresses(id));
//        if(dto == null) {
//            throw new RuntimeException(String.format("User not found: %s", id));
//        }
        model.addAttribute("dto", dto);
        return "updateUser";
    }

    @PostMapping/*("/{userId}")*/
    public ModelAndView submitUpdateUserForm(@ModelAttribute("dto") CreateOrUpdateUserDto dto/*,
                                         @PathVariable("userId") Long userId ,
                                         Model model*/) {
        //model.addAttribute("users", userService.findAll());
        userService.save(UserMapper.INSTANCE.toEntity(dto));
        return new ModelAndView("redirect:/users"/*, model.asMap()*/);
    }

    @GetMapping("/create")
    public String showCreateUserForm(Model model) {
        model.addAttribute("dto", new CreateOrUpdateUserDto());
        return "addUser";
    }

    @PostMapping("/create")
    public ModelAndView submitAddUserForm(@ModelAttribute("dto") CreateOrUpdateUserDto dto,
                                    Model model) {
        final User user = userService.save(UserMapper.INSTANCE.toEntity(dto));
        model.addAttribute("successMsg", String.format("User with login: %s was successfully created",
                user.getLogin()));
        return new ModelAndView("redirect:/users", model.asMap());
    }

    @InitBinder
    public void allowEmptyDateBinding( WebDataBinder binder )
    {
        // tell spring to set empty values as null instead of empty string.
        binder.registerCustomEditor( String.class, new StringTrimmerEditor( true ));
    }
}
