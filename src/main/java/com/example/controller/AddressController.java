//package com.example.controller;
//
//import com.example.dto.CreateOrUpdateAddressDto;
//import com.example.mappers.AddressMapper;
//import com.example.model.Address;
//import com.example.service.AddressService;
//import org.springframework.beans.propertyeditors.StringTrimmerEditor;
//import org.springframework.stereotype.Controller;
//import org.springframework.ui.Model;
//import org.springframework.web.bind.WebDataBinder;
//import org.springframework.web.bind.annotation.*;
//import org.springframework.web.servlet.ModelAndView;
//
//@Controller
//@RequestMapping("/addresses")
//public class AddressController {
//
//    private final AddressService addressService;
//
//    public AddressController(AddressService addressService) {
//        this.addressService = addressService;
//    }
//
//    @GetMapping
//    public String index(Model model, @ModelAttribute("successMsg") String successMsg) {
//        model.addAttribute("users", addressService.findAll());
//        model.addAttribute("successMsg", successMsg);
//        return "index";
//    }
//
//    @GetMapping("/{addressId}")
//    public String showUpdateAddressForm(Model model, @PathVariable("addressId") Long id) {
//        CreateOrUpdateAddressDto dto = AddressMapper.INSTANCE.toDto(addressService.find(id));
////        if(dto == null) {
////            throw new RuntimeException(String.format("User not found: %s", id));
////        }
//        model.addAttribute("dto", dto);
//        return "updateAddress";
//    }
//
//    @PostMapping/*("/{addressId}")*/
//    public ModelAndView submitUpdateUserForm(@ModelAttribute("dto") CreateOrUpdateAddressDto dto/*,
//                                         @PathVariable("addressId") Long addressId ,
//                                         Model model*/) {
//        //model.addAttribute("addresses", addressService.findAll());
//        Address previousAddress = addressService.find(dto.getId());
//        previousAddress.setCity(dto.getCity());
//        previousAddress.setCountry(dto.getCountry());
//        previousAddress.setStreet(dto.getStreet());
//        addressService.save(previousAddress);
//        return new ModelAndView("redirect:/users"/*, model.asMap()*/);
//    }
//
//    @GetMapping("/create")
//    public String showCreateAddressForm(Model model) {
//        model.addAttribute("dto", new CreateOrUpdateAddressDto());
//        return "addUser";
//    }
//
//    @PostMapping("/create")
//    public ModelAndView submitAddAddressForm(@ModelAttribute("dto") CreateOrUpdateAddressDto dto,
//                                          Model model) {
//        final Address address = addressService.save(AddressMapper.INSTANCE.toEntity(dto));
//        model.addAttribute("successMsg", "Address was successfully created");
//        return new ModelAndView("redirect:/users", model.asMap());
//    }
//
//    @InitBinder
//    public void allowEmptyDateBinding( WebDataBinder binder )
//    {
//        // tell spring to set empty values as null instead of empty string.
//        binder.registerCustomEditor( String.class, new StringTrimmerEditor( true ));
//    }
//}
