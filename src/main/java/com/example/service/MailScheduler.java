package com.example.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class MailScheduler {

    private final MailService mailService;

    public MailScheduler(MailService mailService) {
        this.mailService = mailService;
    }

//    @Scheduled(fixedRate = 30 * 1000) //opóźnienie pomiędzy kolejnymi wywołaniami
//    public void sendMail() {
//        log.info("Sending email...");
//        mailService.send("adkowal777@gmail.com", "This is a simple test!", "Test!!!");
//    }
}
