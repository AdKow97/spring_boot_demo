//package com.example.service;
//
//import com.example.model.Address;
//import com.example.repository.AddressRepository;
//import org.springframework.stereotype.Service;
//
//import java.util.List;
//import java.util.function.Supplier;
//
//@Service
//public class AddressService {
//
//    final private AddressRepository addressRepository;
//
//    public AddressService(AddressRepository addressRepository) {
//        this.addressRepository = addressRepository;
//    }
//
//    public Address find(Long id) {
//        return addressRepository.findById(id).orElseThrow(getAddressNotFoundExceptionSupplier(id));
//    }
//
//    private Supplier<RuntimeException> getAddressNotFoundExceptionSupplier(Long id) {
//        return () -> new RuntimeException(String.format("Address not found: %s", id));
//    }
//
//    public Address save(Address address) {
//        return addressRepository.save(address);
//    }
//
//    public List<Address> findAll() {
//        return addressRepository.findAll();
//    }
//
//    public void delete(Long id) { addressRepository.deleteById(id); }
//}
