package com.example.configuration;

import com.example.filter.AuthorizationFilter;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

import javax.sql.DataSource;

@Configuration
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    private static final String USERS_QUERY = "select login, password, enabled from user where login = ?";

    private static final String ROLES_QUERY =
            "SELECT u.login AS login, r.name FROM user u " +
            "  JOIN user_to_role AS ur ON u.id = ur.user_id " +
            "    JOIN role r ON ur.role_id =  r.id " +
            "      WHERE u.login = ?";

    private final DataSource dataSource;

    public SecurityConfig(DataSource dataSource) {
        this.dataSource = dataSource;
    }

//    @Bean
//    public FilterRegistrationBean<AuthorizationFilter> registerAuthFilter() {
//        FilterRegistrationBean<AuthorizationFilter> filterRegistrationBean =
//                new FilterRegistrationBean<>();
//        filterRegistrationBean.setFilter(new AuthorizationFilter());
//        filterRegistrationBean.addUrlPatterns("/users/*", "/rest/*");
//        return filterRegistrationBean;
//    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.csrf().disable()
                .authorizeRequests()
                .antMatchers("/rest/users/**").permitAll()
//                .antMatchers(HttpMethod.GET,"/users", "/").hasAnyRole("ADMIN", "USER")
//                .antMatchers("/users/**").hasRole("ADMIN")
//                .antMatchers("/rest/users/**").hasRole("ADMIN")
//                .anyRequest().authenticated()
                .anyRequest().authenticated()
                .and()
                .oauth2Login()
//                .formLogin()
//                .and()
//                .httpBasic();
        .and()
        .logout();
    }

//    @Override
//    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
//        auth.jdbcAuthentication()
//                .passwordEncoder(passwordEncoder())
//                .rolePrefix("ROLE_")
//                .dataSource(dataSource)
//                .usersByUsernameQuery(USERS_QUERY)
//                .authoritiesByUsernameQuery(ROLES_QUERY);
//    }

    //poprzednia wersja
//    @Override
//    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
//        auth.inMemoryAuthentication().passwordEncoder(passwordEncoder())
//                .withUser("admin")
//                .password(passwordEncoder().encode("password"))
//                .roles("ADMIN")
//                .and()
//                .withUser("user")
//                .password(passwordEncoder().encode("password"))
//                .roles("USER");
//    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }
}
