package com.example.rest;

import com.example.dto.CreateOrUpdateUserDto;
import com.example.mappers.UserMapper;
import com.example.service.UserService;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/rest/users")
public class UserRestController {

    private final UserService userService;

    public UserRestController(UserService userService) {
        this.userService = userService;
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.ACCEPTED)
    public void delete(@PathVariable("id") Long id) {
        userService.delete(id);
    }

    @GetMapping("/{id}")
    public @ResponseBody CreateOrUpdateUserDto find(@PathVariable("id") Long id) {
        return UserMapper.INSTANCE.toDto(userService.find(id));
    }
}
