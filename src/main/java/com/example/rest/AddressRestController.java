//package com.example.rest;
//
//import com.example.service.AddressService;
//import org.springframework.http.HttpStatus;
//import org.springframework.web.bind.annotation.*;
//
//@RestController
//@RequestMapping("/rest/addresses")
//public class AddressRestController {
//
//    private final AddressService addressService;
//
//    public AddressRestController(AddressService addressService) {
//        this.addressService = addressService;
//    }
//
//    @DeleteMapping("/{id}")
//    @ResponseStatus(HttpStatus.ACCEPTED)
//    public void delete(@PathVariable("id") Long id) {
//        addressService.delete(id);
//    }
//}
