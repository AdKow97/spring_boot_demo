//package com.example.mappers;
//
//import com.example.dto.CreateOrUpdateAddressDto;
//import com.example.model.Address;
//import org.mapstruct.Mapper;
//import org.mapstruct.factory.Mappers;
//
//@Mapper
//public interface AddressMapper {
//
//    AddressMapper INSTANCE = Mappers.getMapper(AddressMapper.class);
//
//    Address toEntity(CreateOrUpdateAddressDto dto);
//
//    CreateOrUpdateAddressDto toDto(Address entity);
//}
