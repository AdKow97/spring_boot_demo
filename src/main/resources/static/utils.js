function onEditClicked(userId) {
    window.location.href = '/users/' + userId;
}

// function onEditClicked(addressId) {
//     window.location.href = '/addresses/' + addressId;
// }

function logout() {
    window.location.href = '/logout';
}

function onCreateUserClicked() {
    window.location.href = '/users/create';
}

// function onCreateAddressClicked() {
//     window.location.href = '/addresses/create';
// }

function showOrHideSuccessModal(successMsg) {
    return successMsg.length > 0;
}

function initForm() {
    $('#successModal')
        .toggle(showOrHideSuccessModal($('#successMsg').val()));
}

function closeModal() {
    $('#successModal').toggle(false);
}

$(document).ready(() => {

    initForm();

    $(".button-delete").click((event) => {
        console.info('Element clicked with id: ', event.target.id);
        const id = event.target.getAttribute('data-id');
        $.ajax(
            {
                url: '/rest/users/' + id,
                method: 'DELETE'
            }
        )
            .then(() => {
                console.log('Element was deleted: ', id);
                window.location.href = '/users';
            })
            .catch((error) => {
                console.error('Error during user deletion: ', error);
            });
    })
});
