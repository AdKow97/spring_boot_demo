ALTER TABLE user_to_role
    ADD CONSTRAINT user_to_role_uc UNIQUE (user_id, role_id);