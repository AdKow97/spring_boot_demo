CREATE TABLE user_to_role(
    user_id bigint(20) NOT NULL,
    role_id bigint(20) NOT NULL,
    FOREIGN KEY (user_id) REFERENCES user(id),
    FOREIGN KEY (role_id) REFERENCES role(id),
    INDEX user_id_idx (user_id),
    INDEX role_id_idx (role_id)
) ENGINE=INNODB;